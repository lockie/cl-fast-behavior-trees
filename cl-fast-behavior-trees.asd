(defsystem "cl-fast-behavior-trees"
  :version "0.1.0"
  :author "Andrew Kravchuk <awkravchuk@gmail.com>"
  :license "MIT"
  :homepage "https://gitlab.com/lockie/cl-fast-behavior-trees"
  :depends-on (#:alexandria #:cl-fast-ecs #:let-plus)
  :serial t
  :components ((:module "src"
                :components
                ((:file "package")
                 (:file "nodes")
                 (:file "trees")
                 (:file "dump")
                 (:file "standard-nodes"))))
  :description "Behavior tree pattern implementation for use in games AI."
  :in-order-to ((test-op (test-op "cl-fast-behavior-trees/tests"))))

(defsystem "cl-fast-behavior-trees/tests"
  :author "Andrew Kravchuk <awkravchuk@gmail.com>"
  :license "MIT"
  :depends-on (#:cl-fast-behavior-trees #:parachute)
  :serial t
  :components ((:module "tests"
                :components
                ((:file "package")
                 (:file "main")
                 (:file "trees"))))
  :description "Test system for cl-fast-behavior-trees"
  :perform (test-op (op c) (uiop:symbol-call :cl-fast-behavior-trees/tests '#:run)))
