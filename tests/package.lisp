(in-package #:cl-user)


(defpackage cl-fast-behavior-trees/tests
  (:use #:cl
        #:cl-fast-behavior-trees
        #:parachute)
  (:export #:run))
