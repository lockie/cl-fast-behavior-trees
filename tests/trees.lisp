(in-package #:cl-fast-behavior-trees/tests)


(define-test trees
  :parent cl-fast-behavior-trees)

(define-test tree-ctor
  :parent trees
  (eval
   '(define-behavior-tree test
     ((repeat :name "root")
      ((always-true :name "node")))))
  (ecs:bind-storage)
  (let* ((entity (ecs:make-entity))
         (_ (make-test-behavior-tree entity))
         (entity-structure (ecs:print-entity entity (make-broadcast-stream))))
    (declare (ignore _))
    (true (find '(:behavior-tree-marker :type :test)
                entity-structure :test #'equal))
    (true (find '(:test-root :active 1 :completed 0 :succeeded 0)
                entity-structure :test #'equal))
    (true (find '(:test-node :active 0 :completed 0 :succeeded 0)
                entity-structure :test #'equal))))

(define-test tree-redefinition
  :parent trees
  (eval
   '(define-behavior-tree/debug test4
      ((repeat :name "root")
       ((always-true :name "node")))))
  (eval
   '(define-behavior-tree/debug test4
      ((repeat :name "root")
       ((fallback :name "fall")
        ((always-false :name "node"))
        ((always-true :name "node2"))))))
  (ecs:bind-storage)
  (let* ((entity (ecs:make-entity))
         (_ (make-test4-behavior-tree entity))
         (_ (ecs:run-systems)))
    (declare (ignore _))
    (dump-behavior-tree entity)
    (ecs:print-entity entity)
    (is = 1 (test4-node-completed entity))))

(define-test basic-tree-run
  :parent trees
  (eval
   '(define-behavior-tree test
      ((repeat :name "root")
       ((always-true :name "node")))))
  (ecs:bind-storage)
  (let* ((entity (ecs:make-entity))
         (_ (make-test-behavior-tree entity))
         (_ (ecs:run-systems)))
    (declare (ignore _))
    (is = 1 (test-node-completed entity))
    (is = 1 (test-node-succeeded entity))))

(define-test tree-run
  :parent trees
  (eval
   '(define-behavior-tree test2
     ((repeat :name "root")
      ((fallback :name "fall")
       ((always-false :name "node"))
       ((always-true :name "node2"))))))
  (ecs:bind-storage)
  (let* ((entity (ecs:make-entity))
         (_ (make-test2-behavior-tree entity)))
    (declare (ignore _))
    (ecs:run-systems)
    (is = 0 (test2-root-active entity))
    (is = 1 (test2-fall-active entity))
    (is = 1 (test2-node-completed entity))
    (ecs:run-systems)
    (is = 1 (test2-node2-completed entity))
    (ecs:run-systems)
    (is = 1 (test2-fall-completed entity))
    (is = 1 (test2-root-active entity))))

(define-test node-name-counters
  :parent trees
  (eval
   '(define-behavior-tree test3
     ((repeat :name "root")
      (always-true))))
  (ecs:bind-storage)
  (let* ((entity (ecs:make-entity))
         (_ (make-test3-behavior-tree entity))
         (_ (ecs:run-systems)))
    (declare (ignore _))
    (is = 1 (test3-always-true1-completed entity))
    (is = 1 (test3-root-active entity))))
