(in-package #:cl-user)


(defpackage cl-fast-behavior-trees
  (:documentation "**NOTE: this software is of alpha quality, and the API is
subject to change.**

`cl-fast-behavior-trees` is a Common Lisp macro library providing an
implementation of the Behavior Tree pattern, primarily focused
on speed and interactive development.

Behavior Tree is mathematical model of plan of execution used in robotics and
video games. It allows to describe a complex task composed of simple ones in a
modular fashion, and it is easy to understand which makes it less error prone
and more popular in game developer community.")
  (:nicknames #:fbt)
  (:use #:cl #:let-plus)
  (:import-from #:alexandria
                #:array-index #:copy-hash-table #:format-symbol #:if-let
                #:make-keyword #:parse-body #:string-designator #:symbolicate
                #:when-let)
  ;; nodes.lisp
  (:export
   #:define-behavior-tree-node)
  ;; trees.lisp
  (:export
   ;; convenience
   #:make-behavior-tree
   #:delete-behavior-tree
   ;; helpers
   #:complete-node
   #:child-completed-p
   #:child-succeeded-p
   #:activate-child
   #:reset-children
   #:delete-tree
   ;; actual stuff
   #:define-behavior-tree
   #:define-behavior-tree/debug
   #:define-behavior-tree-from-spec)
  ;; dump.lisp
  (:export
   #:dump-behavior-tree
   #:dump-behavior-tree/file
   #:dump-behavior-tree/picture))
