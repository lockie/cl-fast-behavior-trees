(in-package #:cl-fast-behavior-trees)


(ecs:defcomponent behavior-tree-marker
  (type :|| :type keyword))

(defgeneric make-behavior-tree (tree entity))
(defgeneric delete-behavior-tree (tree entity))

(defgeneric %dump-tree (tree entity))

(defmacro %muffle-redefinition-warnings (&body body)
  "Evaluate the body so that redefinition warnings will not be signaled."
  #+allegro `(excl:without-redefinition-warnings
               ,@body)
  #+(or ccl mcl) `(let ((ccl::*warn-if-redefine* nil)
                        (ccl::*record-source-file* nil))
                    ,@body)
  #+clisp `(let ((custom:*suppress-check-redefinition* t))
             ,@body)
  #+lispworks `(let ((dspec:*redefinition-action* :quiet))
                 ,@body)
  #+sbcl `(handler-bind ((sb-kernel:redefinition-warning #'muffle-warning))
            ,@body)
  #-(or allegro ccl clisp lispworks mcl sbcl) `(progn ,@body))

(define-let+-expansion (&helper (name lambda-list &body function-body)
                           :uses-value? nil)
  `(flet ((,name ,lambda-list ,@function-body))
     (declare (ignorable (function ,name))
              (inline ,name))
     ,@let-plus::body))

(declaim (inline %parse-node)
         (ftype (function (symbol list hash-table)
                          (values symbol list string-designator symbol list))
                %parse-node))
(defun %parse-node (tree-name node-spec type-counters)
  (let+ (((type-and-options &rest children) node-spec)
         (type (if (typep type-and-options 'symbol)
                   type-and-options (first type-and-options)))
         (options (if (typep type-and-options 'symbol)
                      nil (rest type-and-options)))
         (name (or (getf options :name)
                   (symbolicate
                    type
                    (write-to-string
                     (incf (the fixnum (gethash type type-counters 0)))))))
         (node-name (symbolicate tree-name :- (string-upcase name))))
    (values type options name node-name children)))

(declaim (ftype (function (symbol list hash-table list list list) list)
                %tree-node-components))
(defun %tree-node-components
    (tree-name spec type-counters node-names node-types node-options)
  (declare (type list node-names))
  (let+ (((&values type options &ign node-name children)
          (%parse-node tree-name spec type-counters))
         (options* (copy-list options))
         (slots (symbol-value
                 (format-symbol :cl-fast-behavior-trees "%~a-SLOTS" type))))
    (when-let (forbidden-slot-names
               (intersection
                (mapcar #'first slots) '(completed succeeded active)))
      (error "~a node contains forbidden slot names: ~a"
             node-name forbidden-slot-names))
    (when-let (duplicate-name (find node-name node-names :test #'eq))
      (error "Duplicate node name: ~a" duplicate-name))
    (nconc node-names (list node-name))
    (nconc node-types (list type))
    (remf options* :name)
    (nconc node-options (list options*))
    `(progn
       (ecs:defcomponent ,node-name
         (active 0 :type bit)
         (completed 0 :type bit)
         (succeeded 0 :type bit)
         ,@slots)
       ,@(mapcar
          #'(lambda (child-spec)
              (%tree-node-components tree-name child-spec type-counters
                                     node-names node-types node-options))
          children))))

(declaim (ftype (function (symbol list symbol hash-table symbol) list)
                %child-accessors))
(defun %child-accessors (tree-name children suffix type-counters entity)
  (labels ((traverse-children (node)
             (let+ (((&values &ign &ign &ign node-name children)
                     (%parse-node tree-name node type-counters)))
               (dolist (child children)
                 (traverse-children child))
               `((,(symbolicate node-name suffix) ,entity)))))
    (loop :for child :in children
          :for i :of-type fixnum :from 0
          :for accessor := (traverse-children child)
          :collecting (cons i accessor))))

(declaim (ftype (function (symbol list hash-table symbol) list)
                %reset-children))
(defun %reset-children (tree-name spec type-counters entity)
  (let+ (((&ign &rest children) spec))
    (mapcan
     #'(lambda (child)
         (let+ (((&values type options &ign child-name &ign)
                 (%parse-node tree-name child type-counters))
                (slots
                 (symbol-value
                  (format-symbol :cl-fast-behavior-trees "%~a-SLOTS" type))))
           `((setf (,(symbolicate child-name :-completed) ,entity) 0)
             (,(symbolicate :with- child-name) nil ,entity
              (setf ,@(mapcan
                       #'(lambda (slot)
                           `(,(first slot)
                             ,(or (getf options (make-keyword (first slot)))
                                  (second slot))))
                       slots)))
             ,@(%reset-children tree-name child type-counters entity))))
     children)))

(declaim (ftype (function (symbol list hash-table symbol) list) %delete-node))
(defun %delete-node (tree-name spec type-counters entity)
  (let+ (((&values &ign &ign &ign node-name children)
          (%parse-node tree-name spec type-counters)))
    `(progn
       (,(symbolicate :delete- node-name) ,entity)
       ,@(mapcar #'(lambda (child)
                     (%delete-node tree-name child type-counters entity))
                 children))))

(declaim
 (ftype (function (symbol (or null symbol) list list hash-table boolean) list)
        %tree-node-systems))
(defun %tree-node-systems
    (tree-name parent node-spec tree-spec type-counters debug)
  (let+ (((&values type &ign &ign node-name children)
          (%parse-node tree-name node-spec type-counters))
         (initial-type-counters (copy-hash-table type-counters))
         (system-opts
          (symbol-value
           (format-symbol :cl-fast-behavior-trees "%~a-OPTIONS" type)))
         (components-ro (getf system-opts :components-ro))
         (components-rw (getf system-opts :components-rw))
         (system-opts* (copy-list system-opts))
         (slots
          (symbol-value
           (format-symbol :cl-fast-behavior-trees "%~a-SLOTS" type)))
         (body
          (symbol-value
           (format-symbol :cl-fast-behavior-trees "%~a-BODY" type)))
         ((&values body-forms decl-forms docstring)
          (parse-body body :documentation t))
         (entity (intern "ENTITY")))
    (remf system-opts* :components-ro)
    (remf system-opts* :components-rw)
    `(progn
       ,@(mapcar
          #'(lambda (child-spec)
              (%tree-node-systems
               tree-name node-name child-spec tree-spec type-counters debug))
          children)
       (when (ecs:system-exists-p ',node-name)
         (ecs:delete-system ',node-name))
       (ecs:defsystem ,node-name
         (:components-ro (,@components-ro)
          :components-rw (,node-name ,@components-rw)
          ,@system-opts*)
         ,docstring
         (let+ (((&symbol-macrolet
                  ,(symbolicate type :-active)
                  ,(symbolicate node-name :-active)))
                ((&symbol-macrolet
                  ,(symbolicate type :-completed)
                  ,(symbolicate node-name :-completed)))
                ((&symbol-macrolet
                  ,(symbolicate type :-succeeded)
                  ,(symbolicate node-name :-succeeded)))
                ,@(loop :for slot :in slots
                        :for slot-name := (first slot)
                        :collecting
                           `((&symbol-macrolet
                              ,(symbolicate type :- slot-name)
                              ,(symbolicate node-name :- slot-name))))
                ((&helper complete-node (success)
                   (setf ,(symbolicate node-name :-active) 0
                         ,(symbolicate node-name :-completed) 1
                         ,(symbolicate node-name :-succeeded)
                         (if success 1 0))
                   ,(when parent
                      `(setf (,(symbolicate parent :-active) ,entity) 1))
                   nil))
                ((&helper child-completed-p (i)
                   ,(when children
                      `(plusp
                        (case i
                          ,@(%child-accessors tree-name children :-completed
                             (copy-hash-table initial-type-counters) entity)
                          (otherwise 0))))))
                ((&helper child-succeeded-p (i)
                   ,(when children
                      `(plusp
                        (case i
                          ,@(%child-accessors tree-name children :-succeeded
                             (copy-hash-table initial-type-counters) entity)
                          (otherwise 0))))))
                ((&helper activate-child (i)
                   ,(when children
                      `(case i
                         ,@(mapcar
                            #'(lambda (accessor)
                                `(,(first accessor)
                                  (setf ,(second accessor) 1)))
                            (%child-accessors
                             tree-name children :-active
                             (copy-hash-table initial-type-counters)
                             entity))))))
                ((&helper reset-children ()
                   ,@(%reset-children tree-name node-spec
                                      (copy-hash-table initial-type-counters)
                                      entity)))
                ;; TODO put that in separate function to speed up code generation
                ((&helper delete-tree ()
                   ,(%delete-node tree-name tree-spec
                                  (make-hash-table :test #'eq) entity)))
                (children-count ,(length children)))
           (declare (ignorable children-count))
           (when (plusp ,(symbolicate node-name :-active))
             ,(when debug
                `(format t "running ~a node ~s for entity ~a~%"
                         ',type ',node-name ,entity))
             ,@decl-forms
             ,@body-forms))))))

(declaim
 (ftype (function (symbol (or null symbol string) list hash-table symbol) list)
        %tree-node-dump))
(defun %tree-node-dump (tree-name parent spec type-counters entity)
  (let+ (((type-and-options &rest children) spec)
         (type (if (typep type-and-options 'symbol)
                   type-and-options (first type-and-options)))
         (options (if (typep type-and-options 'symbol)
                      nil (rest type-and-options)))
         (name (or (getf options :name)
                   (symbolicate
                    type
                    (write-to-string
                     (incf (the fixnum (gethash type type-counters 0)))))))
         (print-name (if-let (named (getf options :name))
                       (format nil "~:@(~a~)~%~a" named type)
                       (string-upcase name)))
         (node-name (symbolicate tree-name :- (string-upcase name)))
         (special-p (find (make-keyword type)
                          '(:repeat :repeat-until-fail :invert :fallback
                            :sequence :parallel)
                          :test #'eq)))
    `(progn
       (setf
        output
        (format nil node-format output ',name ,special-p ,print-name
                (plusp (,(symbolicate node-name :-active) ,entity))))
       ,(when parent
          `(setf output (format nil "~a \"~a\" -> \"~a\"; "
                                output ',parent ',name)))
       ,@(mapcar
          #'(lambda (child-spec)
              (%tree-node-dump tree-name name child-spec type-counters entity))
          children))))

(declaim (ftype (function (symbol list hash-table symbol) list) %tree-dump))
(defun %tree-dump (tree-name spec type-counters entity)
  `(let ((node-format "~a\"~a\" [shape=~:[rect~;oval~],
label=\"~a\",style=\"~:[~;filled~]\"]; ")
         (output
           (format
            nil
            "digraph behavior_tree_~a { node [fontname=\"SourceSans3\"]; "
            ,entity)))
     ,(%tree-node-dump tree-name nil spec type-counters entity)
     (format nil "~a}" output)))

(declaim (ftype (function (symbol list &key (:debug boolean)) list)
                %define-behavior-tree))
(defun %define-behavior-tree (tree-name spec &key (debug nil))
  (let+ ((node-names (list nil))
         (node-types (list nil))
         (node-options (list nil))
         (tree-type (make-keyword tree-name))
         (entity (intern "ENTITY")))
    `(progn
       ,(%tree-node-components tree-name spec (make-hash-table :test #'eq)
                               node-names node-types node-options)
       ,(%tree-node-systems
         tree-name nil spec spec (make-hash-table :test #'eq) debug)
       (defun ,(symbolicate :make- tree-name :-behavior-tree) (,entity)
         (when (has-behavior-tree-marker-p ,entity)
           (error "The entity ~a already has ~a behavior tree assigned"
                  ,entity (behavior-tree-marker-type ,entity)))
         (make-behavior-tree-marker ,entity :type ,tree-type)
         ,@(mapcar #'(lambda (node options)
                       (when node
                         `(,(symbolicate :make- node) ,entity ,@options)))
                   node-names node-options)
         (setf (,(symbolicate (first (rest node-names)) :-active) ,entity)
               1)
         nil)
       (defun ,(symbolicate :delete- tree-name :-behavior-tree) (,entity)
         ,(%delete-node tree-name spec (make-hash-table :test #'eq) entity)
         (delete-behavior-tree-marker ,entity))
       (defmethod make-behavior-tree ((tree (eql ,tree-type)) ,entity)
         (,(symbolicate :make- tree-name :-behavior-tree) ,entity))
       (defmethod delete-behavior-tree ((tree (eql ,tree-type)) ,entity)
         (,(symbolicate :delete- tree-name :-behavior-tree) ,entity))
       (defmethod %dump-tree ((tree (eql ,tree-type)) ,entity)
         ,(%tree-dump tree-name spec (make-hash-table :test #'eq) entity))
       (setf (gethash ,tree-type *maybe-rebuild-tree-fns*)
             #'(lambda (node-type)
                 (when (find node-type ',(remove nil node-types) :test #'eq)
                     (eval
                      (%muffle-redefinition-warnings
                        (%define-behavior-tree
                         ',tree-name ',spec :debug ,debug)))))))))

(defun import-standard-node-types-symbols ()
  "HACK for standard node types to work in trees from other packages"
  (import '(cl-fast-behavior-trees::repeat-active
            cl-fast-behavior-trees::repeat-until-fail-active
            cl-fast-behavior-trees::invert-active
            cl-fast-behavior-trees::fallback-active
            cl-fast-behavior-trees::fallback-current
            cl-fast-behavior-trees::sequence-active
            cl-fast-behavior-trees::sequence-current
            cl-fast-behavior-trees::random-probability)))

(defmacro define-behavior-tree (name spec)
  (import-standard-node-types-symbols)
  (%define-behavior-tree name spec :debug nil))

(defmacro define-behavior-tree/debug (name spec)
  (import-standard-node-types-symbols)
  (%define-behavior-tree name spec :debug t))

(defun define-behavior-tree-from-spec (name spec &key debug)
  (import-standard-node-types-symbols)
  (eval (%define-behavior-tree name spec :debug debug)))
