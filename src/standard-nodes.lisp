(in-package #:cl-fast-behavior-trees)


(define-behavior-tree-node repeat ()
  "Constantly reruns its only child."
  (when (child-completed-p 0)
    (reset-children))
  (setf repeat-active 0)
  (activate-child 0))

(define-behavior-tree-node repeat-until-fail ()
  "Constantly reruns its only child until it fails."
  (when (child-completed-p 0)
    (if (child-succeeded-p 0)
        (reset-children)
        (return-from ecs::current-entity (complete-node t))))
  (setf repeat-until-fail-active 0)
  (activate-child 0))

(define-behavior-tree-node invert ()
  "Succeeds only if its only child fails and vice versa."
  (when (child-completed-p 0)
    (return-from ecs::current-entity
      (complete-node (not (child-succeeded-p 0)))))
  (setf invert-active 0)
  (activate-child 0))

(define-behavior-tree-node fallback
    ((current 0 :type array-index))
  "Fallback is like an OR logical element. AKA selector."
  (when (child-completed-p fallback-current)
    (if (child-succeeded-p fallback-current)
        (return-from ecs::current-entity (complete-node t))
        (when (>= (incf fallback-current) children-count)
          (return-from ecs::current-entity (complete-node nil)))))
  (setf fallback-active 0)
  (activate-child fallback-current))

(define-behavior-tree-node sequence
    ((current 0 :type array-index))
  "Sequence node is like an AND logical element."
  (when (child-completed-p sequence-current)
    (if (not (child-succeeded-p sequence-current))
        (return-from ecs::current-entity (complete-node nil))
        (when (>= (incf sequence-current) children-count)
          (return-from ecs::current-entity (complete-node t)))))
  (setf sequence-active 0)
  (activate-child sequence-current))

(define-behavior-tree-node parallel
    ((started 0 :type bit))
  "Parallel node runs all of its children simultaneously and fails only when
all of its children fail."
  (if (zerop parallel-started)
      (progn
        (setf parallel-started 1)
        (dotimes (child children-count)
          (activate-child child)))
      (complete-node
       (loop
         :for success :of-type boolean := nil
         :for child :of-type array-index :from 0 :below children-count
         :unless (child-completed-p child) :do (return-from ecs::current-entity)
         :do (setf success (or success (child-succeeded-p child)))
         :finally (return success)))))

(define-behavior-tree-node random
    ((probability 0.5 :type single-float))
  "Random node succeeds with given probability (from 0 to 1)."
  (complete-node (< (random 1.0) random-probability)))

(define-behavior-tree-node always-true ()
  "Unconditionally succeeds."
  (complete-node t))

(define-behavior-tree-node always-false ()
  "Unconditionally fails."
  (complete-node nil))
