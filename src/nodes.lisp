(in-package #:cl-fast-behavior-trees)


(declaim (type hash-table *maybe-rebuild-tree-fns*))
(defvar *maybe-rebuild-tree-fns* (make-hash-table :test #'eq))

(defmacro define-behavior-tree-node (name-and-options (&rest slots) &body body)
  (let* ((name (if (typep name-and-options 'symbol)
                   name-and-options (first name-and-options)))
         (options (if (typep name-and-options 'symbol)
                      nil (rest name-and-options)))
         (slots-var (format-symbol :cl-fast-behavior-trees "%~a-SLOTS" name))
         (options-var
           (format-symbol :cl-fast-behavior-trees "%~a-OPTIONS" name))
         (body-var (format-symbol :cl-fast-behavior-trees "%~a-BODY" name))
         (need-rebuild-p
           (or
            (and (boundp slots-var)
                 (not (equalp slots (symbol-value slots-var))))
            (and (boundp options-var)
                 (not (equalp options (symbol-value options-var))))
            (and (boundp body-var)
                 (not (equalp body (symbol-value body-var))))))
         (*package* (find-package :cl-fast-behavior-trees)))
    `(eval-when (:compile-toplevel :load-toplevel :execute)
       (defparameter ,slots-var ',slots)
       (defparameter ,options-var ',options)
       (defparameter ,body-var ',body)
       ,(when need-rebuild-p
          `(loop :for fn :being :the :hash-values :of *maybe-rebuild-tree-fns*
                 :do (funcall (the function fn) ',name))))))

