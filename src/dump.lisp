(in-package #:cl-fast-behavior-trees)


(declaim (ftype (function (ecs:entity) simple-string) dump-behavior-tree))
(defun dump-behavior-tree (entity)
  (unless (has-behavior-tree-marker-p entity)
    (error "The entity ~a does not have behavior tree assigned" entity))
  (%dump-tree (behavior-tree-marker-type entity) entity))

(declaim (ftype (function (ecs:entity (or pathname string)))
                dump-behavior-tree/file))
(defun dump-behavior-tree/file (entity dot-filename)
  (with-open-file (file dot-filename
                        :direction :output
                        :if-exists :supersede
                        :if-does-not-exist :create)
    (declare (type stream file))
    (format file "~a" (dump-behavior-tree entity))))

(declaim
 (ftype (function (ecs:entity (or pathname string)
                              &key (:format symbol) (:keep-source boolean))
                  (values (or null string) (or null string) fixnum))
        dump-behavior-tree/picture))
(defun dump-behavior-tree/picture (entity filename
                                    &key (format :png) keep-source)
  (uiop:with-temporary-file (:pathname dot-filename
                             :type "dot"
                             :keep keep-source)
    (dump-behavior-tree/file entity dot-filename)
    (uiop:run-program
     (format nil "dot -T~(~a~) ~a > ~a" format dot-filename filename))))
